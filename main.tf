terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.81.0"
    }
  }
}

variable "GCP_SERVICE_ACCOUNT" {
  type = string
}

variable "GCP_SERVICE_KEY" {
  type = string
}

provider "google-beta" {
  project     = "codecollab-io"
  region      = "us-central1"
  credentials = var.GCP_SERVICE_KEY
}

resource "google_artifact_registry_repository" "cc_projects_service" {
  provider = google-beta

  format        = "DOCKER"
  location      = "us-central1"
  repository_id = "cc-projects-service"
}

resource "google_artifact_registry_repository" "cc_auth" {
  provider = google-beta

  format        = "DOCKER"
  location      = "us-central1"
  repository_id = "cc-auth"
}

resource "google_artifact_registry_repository" "cc_editor_api" {
  provider = google-beta

  format        = "DOCKER"
  location      = "us-central1"
  repository_id = "cc-editor-api"
}

resource "google_artifact_registry_repository" "cc_compiler_master" {
  provider = google-beta

  format        = "DOCKER"
  location      = "us-central1"
  repository_id = "cc-compiler-master"
}

resource "google_artifact_registry_repository" "cc_api_gateway" {
  provider = google-beta

  format        = "DOCKER"
  location      = "us-central1"
  repository_id = "cc-api-gateway"
}

resource "google_artifact_registry_repository" "cc_html_server" {
  provider = google-beta

  format        = "DOCKER"
  location      = "us-central1"
  repository_id = "cc-html-server"
}

resource "google_artifact_registry_repository" "cc_api" {
  provider = google-beta

  format        = "DOCKER"
  location      = "us-central1"
  repository_id = "cc-api"
}

resource "google_artifact_registry_repository" "cc_share" {
  provider = google-beta

  format        = "DOCKER"
  location      = "us-central1"
  repository_id = "cc-share"
}

resource "google_artifact_registry_repository" "cc_lti" {
  provider = google-beta

  format        = "DOCKER"
  location      = "us-central1"
  repository_id = "cc-lti"
}